package com.dk.testpoi.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.dk.testpoi.model.Poi;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "testpoi.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<Poi, Integer> poiDao = null;
    private RuntimeExceptionDao<Poi, Integer> poiRuntimeDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, Poi.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, Poi.class, true);
            // after we drop the old databases, we create the new ones
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    public Dao<Poi, Integer> getDao() throws SQLException {
        if (poiDao == null)
            poiDao = getDao(Poi.class);
        return poiDao;
    }

    public RuntimeExceptionDao<Poi, Integer> getPoiDao() {
        if (poiRuntimeDao == null)
            poiRuntimeDao = getRuntimeExceptionDao(Poi.class);
        return poiRuntimeDao;
    }

    @Override
    public void close() {
        super.close();
        poiDao = null;
        poiRuntimeDao = null;
    }
}

package com.dk.testpoi;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.dk.testpoi.db.DatabaseHelper;
import com.dk.testpoi.fragment.PoiDetailsFragment;
import com.dk.testpoi.model.Poi;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private DatabaseHelper databaseHelper = null;

    private ArrayList<Poi> pois;

    private ListView lvPois;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        pois = new ArrayList<>();

        lvPois = (ListView) findViewById(R.id.lvPois);
        lvPois.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Fragment detailsFragment = new PoiDetailsFragment();
                Bundle args = new Bundle();
                args.putParcelable("selected_poi", pois.get(i));
                detailsFragment.setArguments(args);
                getFragmentManager().beginTransaction()
                        .add(R.id.flContainer, detailsFragment)
                        .commit();
            }
        });

        findViewById(R.id.fabAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction()
                        .add(R.id.flContainer, new PoiDetailsFragment())
                        .commit();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadPois();
    }

    public void loadPois() {
        RuntimeExceptionDao<Poi, Integer> poiDao = getDatabaseHelper().getPoiDao();
        pois = new ArrayList<>(poiDao.queryForAll());
        ArrayAdapter<Poi> adapter = new ArrayAdapter<Poi>(this, android.R.layout.simple_list_item_1, pois);
        lvPois.setAdapter(adapter);
    }

    private DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null)
            databaseHelper = new DatabaseHelper(getApplicationContext());
        return databaseHelper;
    }

    @Override
    protected void onDestroy() {
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
        super.onDestroy();
    }
}

package com.dk.testpoi.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.dk.testpoi.MainActivity;
import com.dk.testpoi.R;
import com.dk.testpoi.db.DatabaseHelper;
import com.dk.testpoi.model.Poi;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;

public class PoiDetailsFragment extends Fragment implements View.OnClickListener, OnMapReadyCallback {

    private DatabaseHelper databaseHelper = null;
    private RuntimeExceptionDao<Poi, Integer> poiDao;

    private Poi poi;
    private LatLng coords;

    private EditText etName;
    private EditText etDescription;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        poiDao = getDatabaseHelper().getPoiDao();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_details, container, false);
        etName = (EditText) v.findViewById(R.id.etPoiName);
        etDescription = (EditText) v.findViewById(R.id.etPoiDescription);
        v.findViewById(R.id.ibDelete).setOnClickListener(this);
        v.findViewById(R.id.ibMap).setOnClickListener(this);
        v.findViewById(R.id.ibSave).setOnClickListener(this);

        Bundle args = getArguments();
        if (args != null && args.containsKey("selected_poi")) {
            poi = args.getParcelable("selected_poi");
            etName.setText(poi.name);
            etDescription.setText(poi.description);
            coords = new LatLng(poi.latitude, poi.longitude);
        }
        return v;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            MapFragment mapFragment = (MapFragment) getChildFragmentManager().findFragmentByTag("map");
            if (mapFragment != null && mapFragment.isVisible())
                getChildFragmentManager().beginTransaction().remove(mapFragment).commit();
            else
                closeFragment();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibDelete:
                if (poi != null)
                    poiDao.delete(poi);
                closeFragment();
                break;
            case R.id.ibMap:
                MapFragment mapFragment = new MapFragment();
                getChildFragmentManager().beginTransaction().add(R.id.flDetailsContainer, mapFragment, "map").commit();
                mapFragment.getMapAsync(PoiDetailsFragment.this);
                break;
            case R.id.ibSave:
                if (TextUtils.isEmpty(etName.getText())) {
                    Toast.makeText(getActivity(), R.string.no_name_msg, Toast.LENGTH_SHORT).show();
                    return;
                }
                savePoi();
                break;
        }
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        if (coords != null)
            map.addMarker(new MarkerOptions().position(coords));
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                map.clear();
                map.addMarker(new MarkerOptions().position(point));
                coords = point;
            }
        });
    }

    private void savePoi() {
        if (poi == null)
            poi = new Poi();
        poi.name = etName.getText().toString();
        poi.description = TextUtils.isEmpty(etDescription.getText()) ? "" : etDescription.getText().toString();
        if (coords != null) {
            poi.latitude = coords.latitude;
            poi.longitude = coords.longitude;
        }
        poiDao.createOrUpdate(poi);
        closeFragment();
    }

    private DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null)
            databaseHelper = new DatabaseHelper(getActivity().getApplicationContext());
        return databaseHelper;
    }

    private void closeFragment() {
        ((MainActivity) getActivity()).loadPois();
        getActivity().getFragmentManager().beginTransaction().remove(this).commit();
    }

    @Override
    public void onDestroy() {
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
        super.onDestroy();
    }
}
